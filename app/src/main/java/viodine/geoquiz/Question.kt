package viodine.geoquiz


class Question
{
    var id: Int = 0
    var question: String = ""
    var answer: String = ""

    constructor(question: String, answer: String) {
        this.question = question
        this.answer = answer
    }

    constructor(){
    }
}