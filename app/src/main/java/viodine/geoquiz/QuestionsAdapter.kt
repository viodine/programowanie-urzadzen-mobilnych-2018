package viodine.geoquiz

import android.content.Context
import android.content.Intent
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import kotlinx.android.synthetic.main.question_row.view.*

class QuestionsAdapter(private val questions: ArrayList<Question>, var context: Context) : RecyclerView.Adapter<QuestionsAdapter.ViewHolder>() {

    private val db = DatabaseHandler(context)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.question_row, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int = questions.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val txt = questions[position].question
        val ans = questions[position].answer
        holder.question.text = questions[position].question

        holder.question.setOnClickListener {
            val intent = Intent(context, UpdateActivity::class.java)
            intent.putExtra("UPDATE_QUESTION", txt)
            intent.putExtra("UPDATE_ANSWER", ans)
            context.startActivity(intent)
        }

        holder.question.setOnLongClickListener {
            db.deleteData(txt)
            holder.question.text = ""
            Notification("Question has been deleted!",context).showToast()
            false
        }
    }


    class ViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
        var question: TextView = itemView.question
    }

}
