package viodine.geoquiz

import android.content.ContentValues
import android.content.Context
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.widget.Button
import kotlinx.android.synthetic.main.activity_questions.*

class QuestionsActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_questions)

        var db = DatabaseHandler(this)
        val questions = db.readData()

        val addButton = findViewById<Button>(R.id.mAddButton)
        addButton.setOnClickListener {
            val intent = Intent(this, AddActivity::class.java)
            this.startActivity(intent)
        }

        recyclerView.apply {
            layoutManager = LinearLayoutManager(this@QuestionsActivity)
            adapter = QuestionsAdapter(questions, this@QuestionsActivity)
        }

    }
}
