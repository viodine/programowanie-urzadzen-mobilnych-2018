package viodine.geoquiz

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.Switch

class UpdateActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_update)

        val updatedQuestion = findViewById<EditText>(R.id.updatedQuestion)
        val updatedAnswer = findViewById<Switch>(R.id.updatedAnswer)
        val updateQuestion = findViewById<Button>(R.id.updateQuestion)

        val txt = intent.getStringExtra("UPDATE_QUESTION")
        var answer = intent.getStringExtra("UPDATE_ANSWER")
        var updatedAnswerString: String = ""

        updatedQuestion.setText(txt)
        if (answer == "true") {
            updatedAnswer.isChecked = true
        }

        updateQuestion.setOnClickListener {
            if (updatedQuestion.text.isNotEmpty())
                updatedAnswerString = if (updatedAnswer.isChecked) {
                    "true"
                } else {
                    "false"
                }
            val db = DatabaseHandler(this)
            db.updateData(txt, answer, updatedQuestion.text.toString(), updatedAnswerString)
        }
    }
}
