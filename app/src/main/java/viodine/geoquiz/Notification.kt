package viodine.geoquiz

import android.app.AlertDialog
import android.content.Context
import android.view.Gravity
import android.widget.Toast


class Notification(var text: String, var context: Context) {

    private var toast: Toast = Toast.makeText(context, text, Toast.LENGTH_SHORT)


    fun showToast() {
        toast.setGravity(Gravity.TOP, 0, 10)
        toast.show()
    }

     fun showDialog() {
        val results = AlertDialog.Builder(context)
        results.setTitle("Gratulacje!")
        results.setMessage(text.toString())
        results.show()
    }
}