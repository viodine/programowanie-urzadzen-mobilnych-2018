package viodine.geoquiz

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.ImageButton
import android.widget.TextView

private const val SCORE_CONTENT = "viodine.geoquiz.SCORE"
private const val TOKENS_CONTENT = "viodine.geoquiz.TOKENS"
const val CHEATED_QUESTION = "viodine.geoquiz.QUESTION"

class QuizActivity : AppCompatActivity() {

    private var mQuestionBank: ArrayList<Question> = ArrayList()
    private var mCurrentIndex: Int = 0
    private var mScore: Int = 0
    private var mTokens: Int = 3

    private var questionFrame: TextView? = null
    private var scoreFrame: TextView? = null
    private var tokensFrame: TextView? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_quiz)

        var db = DatabaseHandler(this)
        mQuestionBank = db.readData()

        val tButton = findViewById<ImageButton>(R.id.mTrueButton)
        val fButton = findViewById<ImageButton>(R.id.mFalseButton)

        val pButton = findViewById<Button>(R.id.mPrevButton)

        val cheatButton = findViewById<Button>(R.id.mCheatButton)

        questionFrame = findViewById(R.id.question_frame)
        questionFrame?.text = mQuestionBank[mCurrentIndex].question
        Log.d("SQL", "Testing: $mQuestionBank[mCurrentIndex]")

        scoreFrame = findViewById(R.id.score_frame)
        scoreFrame?.text = mScore.toString()

        tokensFrame = findViewById(R.id.mCheatTokens)
        tokensFrame?.text = mTokens.toString()

        tButton.setOnClickListener { mCheckAnswer(true) }
        fButton.setOnClickListener { mCheckAnswer(false) }

        pButton.setOnClickListener { mUpdateQuestion(false) }

        cheatButton.setOnClickListener { showCheat() }

        questionFrame?.setOnClickListener { mUpdateQuestion(true) }
    }

    override fun onSaveInstanceState(outState: Bundle?) {
        super.onSaveInstanceState(outState)
        outState?.putInt(SCORE_CONTENT, mScore)
        outState?.putInt(TOKENS_CONTENT, mTokens)
    }

    override fun onRestoreInstanceState(savedInstanceState: Bundle?) {
        super.onRestoreInstanceState(savedInstanceState)
        mScore = savedInstanceState?.getInt(SCORE_CONTENT, 0)!!
        mTokens = savedInstanceState.getInt(TOKENS_CONTENT, 0)
        scoreFrame?.text = mScore.toString()
        tokensFrame?.text = mTokens.toString()
    }

    private fun mUpdateQuestion(next: Boolean) {
            if (next) {
                if (mCurrentIndex < mQuestionBank.lastIndex) {
                    mCurrentIndex++
                } else {
                    mCurrentIndex = 0
                }
            } else {
                if (mCurrentIndex > 0) {
                    mCurrentIndex--
                } else {
                    mCurrentIndex = mQuestionBank.lastIndex
                }
            }

        questionFrame?.text = mQuestionBank[mCurrentIndex].question
    }

    private fun mCheckAnswer(userPressedTrue: Boolean) {
        if (userPressedTrue == mQuestionBank[mCurrentIndex].answer.toBoolean()) {
            val correctAnswerToast = Notification("Poprawna odpowiedź!", this)
            correctAnswerToast.showToast()
            mScore++
            scoreFrame?.text = mScore.toString()
            if (mQuestionBank.size <= 1) {
                val intent = Intent(this, SummaryActivity::class.java).apply {
                    putExtra("endScore", mScore)
                    putExtra("endTokens", mTokens)
                }
                this.startActivity(intent)
            } else {
                mQuestionBank.removeAt(mCurrentIndex)
                mUpdateQuestion(true)
            }
        } else {
            val wrongAnswerToast = Notification("Zła odpowiedź :/", this)
            wrongAnswerToast.showToast()
            if (mQuestionBank.size <= 1) {
                val intent = Intent(this, SummaryActivity::class.java).apply {
                    putExtra("endScore", mScore)
                    putExtra("endTokens", mTokens)
                }
                this.startActivity(intent)
            } else {
                mQuestionBank.removeAt(mCurrentIndex)
                mUpdateQuestion(true)
            }
        }
    }

    private fun showCheat() {
        if (mTokens >1) {
            val intent = Intent(this, CheatActivity::class.java).apply {
                val questionNumber = mCurrentIndex
                putExtra(CHEATED_QUESTION, questionNumber)
            }
            mTokens--
            tokensFrame?.text = mTokens.toString()
            startActivity(intent)
        } else {
            val cheatButton = findViewById<Button>(R.id.mCheatButton)
            val tokensFrame = findViewById<TextView>(R.id.mCheatTokens)
            cheatButton.visibility = View.GONE
            tokensFrame.visibility = View.GONE
            mTokens--
            tokensFrame?.text = mTokens.toString()
            startActivity(intent)
        }
    }
}
