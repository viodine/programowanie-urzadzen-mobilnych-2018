package viodine.geoquiz

import android.app.Activity
import android.content.ContentResolver
import android.content.ContentValues
import android.content.Intent
import android.content.pm.PackageManager
import android.drm.DrmStore
import android.net.Uri
import android.os.Build
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.provider.MediaStore
import android.util.Log
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView

class SummaryActivity : AppCompatActivity() {

    private var score: Int = 0
    private var tokens: Int = 0

    private var summaryScore: TextView? = null
    private var summaryTokens: TextView? = null
    private var photoFrame: ImageView? = null

    private val PERMISSION_CODE: Int = 1000
    private val IMAGE_CAPTURE_CODE: Int = 1001
    private var imageUri: Uri? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_summary)

        score = intent.getIntExtra("endScore", 0)
        tokens = intent.getIntExtra("endTokens", 0)

        photoFrame = findViewById(R.id.mPhoto)

        summaryScore = findViewById(R.id.mSummaryScore)
        summaryScore?.text = score.toString() + " / 6"
        summaryTokens = findViewById(R.id.mSummaryTokens)
        summaryTokens?.text = tokens.toString() + " / 3"

        var shareScore = findViewById<Button>(R.id.mShareButton)
        shareScore.setOnClickListener {
            val message = "Wow! I've just score " + score.toString() + " in GeoQuiz. Come and beat me!"
            val intent = Intent()
            intent.action = Intent.ACTION_SEND
            intent.putExtra(Intent.EXTRA_TEXT, message)
            intent.type = "text/plain"
            startActivity(Intent.createChooser(intent, "Share results to: "))
        }

        val takePhoto = findViewById<Button>(R.id.mPhotoButton)
        takePhoto.setOnClickListener {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (checkSelfPermission(android.Manifest.permission.CAMERA) == PackageManager.PERMISSION_DENIED ||
                        checkSelfPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_DENIED  ){
                    val permission = arrayOf(android.Manifest.permission.CAMERA, android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    Log.d("CAMERA", "REQUEST >= 23")
                    requestPermissions(permission, PERMISSION_CODE)
                } else {
                    Log.d("CAMERA", "REQUEST NOT NEEDED")
                    openCamera()
                }
            } else {
                Log.d("CAMERA", "REQUEST < 23")
                openCamera()
            }
        }

    }

    private fun openCamera() {
        val values = ContentValues()
        values.put(MediaStore.Images.Media.TITLE, "Winner Photo")
        values.put(MediaStore.Images.Media.DESCRIPTION, "GeoQuiz Winner")
        imageUri = contentResolver.insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values)
        val cameraIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri)
        startActivityForResult(cameraIntent, IMAGE_CAPTURE_CODE)

    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        when(requestCode) {
            PERMISSION_CODE -> {
                if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED){
                    Log.d("CAMERA", "Camera should open...")
                    openCamera()
                } else {
                    Notification("Permission Denied", this)
                }
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (resultCode == Activity.RESULT_OK){
            photoFrame?.setImageURI(imageUri)
        }
    }
}
