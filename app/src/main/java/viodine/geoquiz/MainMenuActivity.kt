package viodine.geoquiz

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.TextView

class MainMenuActivity : AppCompatActivity() {
    private var apiLevel: TextView? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main_menu)

        val startButton = findViewById<Button>(R.id.mStartButton)

        startButton.setOnClickListener{
            val intent = Intent(this, QuizActivity::class.java)
            this.startActivity(intent)
        }

        val questionsButton = findViewById<Button>(R.id.mQuestionsButton)

        questionsButton.setOnClickListener{
            val intent = Intent(this, QuestionsActivity::class.java)
            this.startActivity(intent)
        }

        val quitButton = findViewById<Button>(R.id.mQuitButton)

        quitButton.setOnClickListener{
            finish()
        }

        apiLevel = findViewById(R.id.mAPILevel)
        apiLevel?.text = "API LEVEL: " + android.os.Build.VERSION.SDK_INT
    }
}
