package viodine.geoquiz

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.Switch

class AddActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add)

        val newQuestion = findViewById<EditText>(R.id.newQuestion)
        val newAnswer = findViewById<Switch>(R.id.newAnswer)
        val addQuestion = findViewById<Button>(R.id.addQuestion)

        var answer = ""

        addQuestion.setOnClickListener {
            if (newQuestion.text.isNotEmpty())
                answer = if (newAnswer.isChecked){
                    "true"
                } else {
                    "false"
                }
                val question = Question(newQuestion.text.toString(), answer)
            val db = DatabaseHandler(this)
            db.insertData(question)
        }
    }
}
