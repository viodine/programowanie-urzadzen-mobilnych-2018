package viodine.geoquiz

import android.content.ContentValues
import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import android.util.Log
import android.widget.Toast

val DATABASE_NAME = "geoquiz.db"
val TABLE_NAME = "questions"
val COL_QUESTION = "question"
val COL_ANSWER = "answer"

class DatabaseHandler(var context: Context) : SQLiteOpenHelper(context, DATABASE_NAME, null,1) {
    override fun onCreate(db: SQLiteDatabase?) {
        val createTable = "CREATE TABLE IF NOT EXISTS " + TABLE_NAME + "(_id INTEGER PRIMARY KEY NOT NULL, " +
                COL_QUESTION + " TEXT, " + COL_ANSWER + " TEXT)"
        db?.execSQL(createTable)
    }

    override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    fun insertData(question: Question) {
        val db = this.writableDatabase
        val cv = ContentValues()
        cv.put(COL_QUESTION, question.question)
        cv.put(COL_ANSWER, question.answer)
        val result = db.insert(TABLE_NAME, null, cv)
        if(result == (-1).toLong())
            Notification("Failed", context).showToast()
        else
            Notification("Success", context).showToast()
    }

    fun readData() : ArrayList<Question> {
        val mQuestionBank : ArrayList<Question> = ArrayList()

        val db = this.readableDatabase
        val sql = "SELECT * FROM " + TABLE_NAME
        val result = db.rawQuery(sql, null)
        if(result.moveToFirst()){
            do {
                val question = Question()
                question.id = result.getString(0).toInt()
                question.question = result.getString(1)
                question.answer = result.getString(2)
                mQuestionBank.add(question)
            }while (result.moveToNext())
        }
        result.close()
        db.close()
        return mQuestionBank
    }

    fun updateData(oldQuestion: String, oldAnswer: String, question: String, answer: String) {
        val db = this.writableDatabase
        val sql = "SELECT * FROM " + TABLE_NAME
        val result = db.rawQuery(sql, null)
        if(result.moveToFirst()){
            do {
                val cv = ContentValues()
                cv.put(COL_QUESTION, question)
                cv.put(COL_ANSWER, answer)
                Log.d("UPGRADE", "${COL_QUESTION}   " + "   ${COL_ANSWER}")
                db.update(TABLE_NAME, cv, "$COL_QUESTION=${result.getString(1)}", null)
                Log.d("UPGRADE", "${question}   " + "   ${answer}")
            } while (result.moveToNext())
        }
        result.close()
        db.close()
    }

    fun deleteData(question: String) {
        val db = this.writableDatabase
        db.delete(TABLE_NAME, COL_QUESTION+"=?", arrayOf(question))
        db.close()
    }
}