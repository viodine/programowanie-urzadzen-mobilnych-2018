package viodine.geoquiz

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.text.method.ScrollingMovementMethod
import android.widget.TextView

class CheatActivity : AppCompatActivity() {

    private val mHintsBank: MutableList<Int> = ArrayList()
    private var cheat_frame: TextView? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_cheat)

        mHintsBank.add(R.string.hint_stolica_polski)
        mHintsBank.add(R.string.hint_stolica_dolnego_slaska)
        mHintsBank.add(R.string.hint_gdynia)
        mHintsBank.add(R.string.hint_mazury)
        mHintsBank.add(R.string.hint_sniezka)
        mHintsBank.add(R.string.hint_wisla)

        val questionNumber = intent.getIntExtra(CHEATED_QUESTION, 0)

        cheat_frame = findViewById(R.id.cheat_frame)
        cheat_frame!!.movementMethod = ScrollingMovementMethod()
        cheat_frame?.text = resources.getString(mHintsBank[questionNumber])

    }
}
